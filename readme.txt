=== URL ShortCodes ===
Contributors: Prakhar
Tags: prakhar, short code, shortcode, template, stylesheet, url, custom, button, enfold
Requires at least: 2.9.0
Tested up to: 5.1.1
Stable tag: rel_1-00
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html


== Description ==
This adds custom functionality to a button to get ute parameters as per the user's choice.

There are custom fields to enter 
1) UTM source 
2) UTM medium
3) UTM campaign
4) UTM term

== Changelog ==

* Version 1.0 - Initial release.

== Installation ==

There are 2 ways of installation. If your setup supports it, you can search for the plugin, by name, within your WordPress control panel and install it from there.
Alternatively, you download the .zip file, unzip it, and copy the resultant `url_short_codes` folder to the `wp-content/plugins/` folder of your WordPress instaltion folder.

== Frequently Asked Questions ==
There are no FAQs at this time. Feel free to suggest some!


== License ==
This plugin uses the GPLv3 license.

